import React, { Component } from 'react';
import './Game.scss';
import GameView from "../../components/GameView/GameView"
import Actions from "../../components/Actions/Actions";

export default class Loader extends Component {


        state = {
            hpj1 : 100,
            hpj2 : 100,
            guerrierLvl : 0,
            infoAction : "Commencer à jouer"
        };

    actionAdversaire = () => {
        return Math.floor(Math.random() * 3);
    };

    verifAttaquer = async (actionJ1, actionJ2) => {
        //alert(actionJ1 +" vs "+ actionJ2);
        if (actionJ1 === "Attaquer" && actionJ2 === 0) {
            await this.setState({hpj1 : this.state.hpj1 - 10});
            await this.setState({hpj2 : this.state.hpj2 - 10});
            await this.setState({infoAction : "Vous avez tous les deux attaqués"});
        } else if (actionJ1 === "Attaquer" && actionJ2 === 1) {
            await this.setState({hpj2 : this.state.hpj2 + 10});
            await this.setState({infoAction : "Votre adversaire a paré votre attaque"});
        } else if (actionJ1 === "Attaquer" && actionJ2 === 2) {
            await this.setState({hpj2 : this.state.hpj2 - 20});
            await this.setState({infoAction : "Vous adversaire charge son attaque, vous attaquez"});
        }
        this.verifWin()
    };

    verifCharger = async (actionJ1, actionJ2) => {
        //alert(actionJ1 +" vs "+ actionJ2);
        if (actionJ1 === "Charger" && actionJ2 === 0) {
            await this.setState({hpj1 : this.state.hpj1 - 20});
            await this.setState({infoAction : "Vous chargez votre attaque, votre adversaire vous attaque"});
        } else if (actionJ1 === "Charger" && actionJ2 === 1) {
            await this.setState({hpj1 : this.state.hpj2 - 5});
            await this.setState({infoAction : "Vous chargez votre attaque, votre adversaire se protege"});
        } else if (actionJ1 === "Charger" && actionJ2 === 2) {
            await this.setState({infoAction : "Magicarpe utilise trempette... mais rien ne se passe..."});
        }
        this.verifWin()
    };

    verifparer = async (actionJ1, actionJ2) => {
        //alert(actionJ1 +" vs "+ actionJ2);
        if (actionJ1 === "Parer" && actionJ2 === 0) {
            await this.setState({hpj1 : this.state.hpj1 + 10});
            await this.setState({infoAction : "Vous avez paré l'attaque"});
        } else if (actionJ1 === "Parer" && actionJ2 === 1) {
            await this.setState({infoAction : "Magicarpe utilise trempette... mais rien ne se passe..."});
        } else if (actionJ1 === "Parer" && actionJ2 === 2) {
            await this.setState({hpj1 : this.state.hpj1 - 5});
            await this.setState({infoAction : "Votre adversaire charge son attaque, vous vous protégez"});
        }
        this.verifWin()
    };

    handleAction =  async(action) => {
        let actionAdversaire = this.actionAdversaire();

        if (action === "Attaquer") {
            await this.verifAttaquer(action, actionAdversaire)
        } else if (action === "Parer") {
            await this.verifparer(action, actionAdversaire)
        } else {
            await this.verifCharger(action,actionAdversaire)
        }
    };

    verifWin = () => {
        if (this.state.hpj2 <= 0) {
            //alert("Vous avez gagné");
            this.nextLvl();
        }
        if (this.state.hpj1 <= 0 ) {
            alert("Vous avez perdu");
            this.remake();
        }
    };

    remake = async () => {
        await this.setState({hpj1 : 100});
        await this.setState({hpj2 : 100});
        await this.setState({guerrierLvl : 0});
        await this.setState({infoAction : "Commencer à jouer"});
    };

    nextLvl = async () => {
        await this.setState({hpj2 : 100});
        await this.setState({guerrierLvl : this.state.guerrierLvl + 1});
        await this.setState({infoAction : "Prochain boss !!!"});
    };

    render() {
        return (
            <header className="Game">  
            <h1>Battle ReactJS</h1>
                <GameView hpJoueur1={this.state.hpj1} hpJoueur2={this.state.hpj2} guerrierLvl={this.state.guerrierLvl} infoAction={this.state.infoAction} />
                <Actions attaquer={this.handleAttack} action={this.handleAction}/>
                <h1>{this.state.test}</h1>
            </header>
        );
    }
}