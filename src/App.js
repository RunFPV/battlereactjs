import React, { Component } from 'react';
import './App.scss';
import Game from './layout/Game/Game'
import Loader from './layout/Loader/Loader';
import {BrowserRouter, Route} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Route exact path="/"  component={Loader} />
          <Route  path="/game"  component={Game} />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
